var aitQuery,$;

 		(function(){

			$ = aitQuery = function(identify)
			{
				return new aitQuery(identify);
			}

			var aitQuery = function (identify)
			{
					var prop = identify.charAt(0);
					var ref = this;

					if(prop === '#')
					{
						var nodeList = new Array();
						nodeList.push(document.getElementById(identify.substr(1,identify.length)));
						return run(nodeList,ref);
					}

					if(prop === '.')
					{
						var nodeList = document.getElementsByClassName(identify.substr(1,identify.length))
						return run(nodeList,ref)
					}
					

					function run(nodeList,ref)
					{
						for (var i = 0 ; i < nodeList.length ; i++) {
							ref[i] = nodeList[i];
						};
						
						ref.length = nodeList.length
						return ref;
					}
			}



			aitQuery.prototype = {
			
				hide: function()
				{	
					for (var i = 0; i < this.length; i++) {
							this[i].style.display = 'none';
						};	

					return this;
				},

				show: function()
				{
					for (var i = 0; i < this.length; i++) {
						this[i].style.display = 'block';
					};

					return this;
				},

				//TODO (ideia) a função de click deve ser o elemento selecionado mais uma prototipação
				//da função de callback dentro de (do objeto atual) - this;

				click: function(callback)
				{
					for (var i = 0; i < this.length; i++) {
						this[i].onclick = callback;
					};
				}
			};
		}
		());


